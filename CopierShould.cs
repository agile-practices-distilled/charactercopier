using Moq;
using Xunit;

namespace CharacterCopier
{
    public class CopierShould
    {
        private const char A_CHARACTER = 'a';

        private readonly SourceSpy _source = new SourceSpy(A_CHARACTER);
        private readonly Mock<IDestination> _destination = new Mock<IDestination>();
        private Copier _copier;

        public CopierShould()
        {
            _copier = new Copier(_source, _destination.Object);
        }
        
        [Fact]
        public void read_a_character_from_source()
        {
            _copier.Copy();

            _destination.Verify(d => d.SetChar(A_CHARACTER));
        }
    }
}