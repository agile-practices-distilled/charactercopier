namespace CharacterCopier
{
    public class Copier
    {
        private readonly ISource _source;
        private readonly IDestination _destination;

        public Copier(ISource source, IDestination destination)
        {
            _source = source;
            _destination = destination;
        }

        public void Copy()
        {
            var inputChar = _source.GetChar();
            _destination.SetChar(inputChar);
        }
    }
}