namespace CharacterCopier
{
    internal class SourceSpy : ISource
    {
        private char _char;

        public SourceSpy(char charToReturn)
        {
            _char = charToReturn;
        }
        
        public char GetChar()
        {
            return _char;
        }
    }
}